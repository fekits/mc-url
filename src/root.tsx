import React from 'react';
import { connect } from 'react-redux';
import UrlParam, { search, hash } from './npm/src';

const store = ({ home, base }: any) => ({ home, base });
const event = (dispatch: any) => {
  return {};
};
function Root(props: any) {
  console.log('hash ', hash.all());
  console.log('search ', search.all());

  return (
    <div className="box">
      <div id="demo" className="floor">
        <div className="box">
          <h4 className="floor-title">
            <span>示例</span>
          </h4>
          <p className="floor-notes">下面我们来看几个相对不同场景下的使用示例！</p>
          <pre className="floor-odd-pre" lang="javascript">
            {`
// @代码着色自豪地采用mc-tinting(预发版)!


// 首先导入插件
import McURL from 'mc-url';

// 新建一个实例
let myUrl = new McURL({
type  : '#',     // 这个实例用于操作hash值,如果要操作search值则设置type为"?";
filter: false,   // 是否开启智能纠错过滤无效参数 比如：#aaa=1&&&&&&&c=2 过滤成#aaa=1&c=2
});

/*
[增,改] 设置一条

给网址设置一个属性名为aaa值为"111"，如果aaa本来就存在则修改它的值，如果本来不存在则会添加一条
*/
myUrl.set({aaa:'111'});
`}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              hash.set({
                aaa: 111
              });
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
// 设置多条
myUrl.set({
  bbb: '222',
  ccc: '333',
  'ddd&':'44?444&444#444'
 });
`}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              hash.set({
                bbb: '222',
                ccc: '333',
                'ddd&': '44?444&444#444'
              });
              // {aaa: '111', bbb: '222', ccc: '333', ddd&: '44?444&444#444'}
              console.log(hash.all());
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
// 删除一条
myUrl.del('aaa');
`}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              hash.del('aaa');
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
// 删除多条
myUrl.del(['bbb','ccc','ddd&']);
`}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              hash.del(['bbb', 'ccc', 'ddd&']);
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
// 删除全部
myUrl.del('*');

/*
  哈哈，是不是一脸懵逼？说的的删除全部参数呢？为什么还有一个#route=home? 刚才发生了什么？

  原因是这步操作刚好触发了本页面的路由啦！

  原网址
  http://junbo.name/plugins/mc-url/#route=demo
  ↓
  我们把整个网页的HASH参数全删了
  http://junbo.name/plugins/mc-url/
  ↓
  网址变化触发本页面路由，路由找不到route参数时自动转向默认路由#route=home
  http://junbo.name/plugins/mc-url/#route=home
*/
`}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              hash.del('*');
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
/*
那如果我不想操作当前的网址，而是想给一条现成的网址加上一些参数然后分享出去怎么办呢？
比如：
let shareUrl = 'http://junbo.name/' -> 'http://junbo.name/#id=1&type=news'
*/

// 原网址
let oldUrl = 'http://junbo.name/';

// 为原网址设置参数后返回为一个新的网址;
let newUrl = myUrl.set([
{
  key: 'id',
  val: 1
},
{
  key: 'type',
  val: 'news'
}
], oldUrl)
alert(newUrl);
// 这个时候如果alert(newUrl) -> 打印的就是 http://junbo.name/#id=1&type=news 啦！

`}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              search.set({
                id: 1,
                type: 'news'
              });
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
// 获取一条
myUrl.get(['aaa']);
`}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              console.log(search.get(['aaa']));
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
  // 先设置一些
  myUrl.set([
    {
      key: 'aaa',
      val: 'this is aaa'
    },
    {
      key: 'bbb',
      val: 222
    }
  ])
  
  // 获取多条并返回数组
  let aHash = myUrl.get(['route', 'aaa', 'bbb']);
  alert(aHash);
  `}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              search.set({ aaa: 'this is aaa', bbb: 222 });
            }}>
            先设置一些 search
          </button>{' '}
          <button
            mcui-btn="@a main"
            onClick={() => {
              search.del('*');
            }}>
            删除全部 search
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
  // 获取多条并返回对象
  let oHash = myUrl.get({ a: 'aaa', b: 'bbb' });
  
  // 为了可以在alert中打印出来看效果先把对象转字符串
  let sHash = JSON.stringify(oHash);
  alert(sHash);
  `}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              let oSearch = search.get({ a: 'aaa', b: 'bbb' });
              console.log(oSearch);
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
// 获取全部参数
let allHash = myUrl.all();

// 为了可以在alert中打印出来看效果先把对象转字符串
let sAllHash = JSON.stringify(allHash);
alert(sAllHash);
  `}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              console.log(search.all());
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
// 判断是否有某个参数
let hasAAA = myUrl.has('aaa');
let hasCCC = myUrl.has('ccc');

alert('当前网址有aaa参数吗？' + hasAAA);
alert('当前网址有ccc参数吗？' + hasCCC);
`}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              let hasAAA = search.has('aaa');
              let hasCCC = search.has('ccc');

              alert('当前网址有aaa参数吗？' + hasAAA);
              alert('当前网址有ccc参数吗？' + hasCCC);
            }}>
            运行实例
          </button>
          <br />
          <br />
          <pre className="floor-odd-pre" lang="javascript">
            {`
  // 操作search跟hash一模一样，省略...

  // 新建一个search实例并开启智能纠错过滤
  let searchUrl = new McURL({
    type: '?',
    filter: true
  });
  
  let lowUrl = 'http://www.junbo.name?aaa=111&&&&&&&&&b&&&&ccc=333&&&b=2';
  let newURL = searchUrl.set({
    key:'b',
    val:'this is bbb !@#$%^^&&**(()'
  },lowUrl);
  alert(newURL);
  `}
          </pre>
          <button
            mcui-btn="@a main"
            onClick={() => {
              let searchUrl = new UrlParam({
                type: '?',
                filter: true
              });

              let lowUrl = 'http://www.junbo.name?aaa=111&&&&&&&&&b&&&&ccc=333&&&b=2';
              console.log(1, lowUrl);
              console.log(searchUrl.all(lowUrl));
              let newURL = searchUrl.set(
                {
                  b: 'this is bbb !@#$%^^&&**(()'
                },
                lowUrl
              );
              console.log(2, newURL);
              console.log(searchUrl.all(newURL));
            }}>
            运行实例
          </button>
          <br />
          <br />
        </div>
      </div>
    </div>
  );
}

export default connect(store, event)(Root);
