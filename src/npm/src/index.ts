import { isString, isArray, isObject } from '@fekit/utils';

// 字符转成对象
const __obj = (sKeys: string = '', filter: boolean = false) => {
  // 去除字符串前面的#号或?号并以&号为切割符将属性内容字符串切割为数组
  const aKeys = sKeys && sKeys.split('&');

  // 先设一个空对象备用
  const oKeys: any = {};

  // 如果数组的长定大于0则继续下一步
  if (aKeys && aKeys.length) {
    // 遍历数组
    for (let i = 0, len = aKeys.length; i < len; i++) {
      const item = aKeys[i];
      // 使用=号切割一组属性与值为数组，第一个为属性，第二个为值，并且将内容解码
      const aItem = item.split('=');
      // 如果对象非空
      if (aItem) {
        const val = decodeURIComponent(aItem[1]);
        // 如果属性名不为空
        if (aItem[0]) {
          // 将属性和值存入到对象中
          oKeys[decodeURIComponent(aItem[0])] = val;
        } else {
          // 如果实例不开启错误属性和冗余&过滤，则将无属性名的或空&代存为"__null__"+下标的临时性属性名。这个是为了可以原样还原网址
          // 比如：   &=333&=&& => 将临时转化为{__null__1:"333",__null__2:"",__null__:"undefined",__null__:"undefined"}
          if (!filter) {
            oKeys['__null__' + i] = val;
          }
        }
      }
    }
  }
  return oKeys;
};
// 拆解网址参数
const __url = (url: any = '', type: any, filter: any = 1): any => {
  let _url = url || window.location.href,
    hash = '',
    route = '',
    search = '';
  _url = _url.replace(/(#.*)/, (_: any, s: any = '') => {
    route = s.replace(/\?(.*)/, (_: any, s: any) => {
      hash = s;
      return '';
    });
    return '';
  });

  _url = _url.replace(/\?(.*)/, (_: any, s: any = '') => {
    search = s;
    return '';
  });

  const res = {
    type,
    preurl: _url + `${type === '?' ? '{params}' : search ? '?' + search : ''}${type === '#' ? '{route}{params}' : route + (hash ? '?' + hash : '')}`,
    route,
    params: type === '?' ? __obj(search, filter) : __obj(hash, filter),
  };
  return res;
};
// 参数拼接字符
const __ins = (params: any, type: any) => {
  let sUrl = '';
  let index = 0;
  for (let item in params) {
    if (params.hasOwnProperty(item)) {
      let key = (index === 0 ? '?' : '&') + encodeURIComponent(item);
      if (/__null__\d+/.test(item)) {
        key = index === 0 ? '' : '&';
      }
      let val = '=' + encodeURIComponent(params[item]);
      if (val === '=undefined') {
        val = '';
      }
      sUrl += key + val;
    }
    index++;
  }
  sUrl.replace(/^&/, type);
  return sUrl;
};
// 重新组装网址
const __res = (type: any, url: any = '', route: any = '', params: any = {}) => {
  const _params = __ins(params, type);
  const _route = route ? route : _params ? '#' : '';
  const _url = url.replace(/{route}/, _route).replace(/{params}/, _params);
  return _url;
};

// 核心
class UrlParam {
  type: any;
  typeName: string;
  filter: any;
  constructor(conf: any = {}) {
    this.type = conf.type;
    this.typeName = this.type === '?' ? 'search' : 'hash';
    this.filter = conf.filter;
  }

  all(url: string = ''): any {
    // 将对象返回出去
    return __url(url, this.type, this.filter)?.params;
  }

  set(params: { [key: string]: string | number | boolean | null | undefined }, url = '') {
    let _url: string = url;
    if (isObject(params)) {
      // 拆解参数
      const { preurl = '', route = '', params: oldParams = {} } = __url(url, this.type, this.filter);
      // 合并参数
      _url = __res(this.type, preurl, route, { ...oldParams, ...params });
      // 替换网址
      if (!url) {
        window.location.href = _url;
      }
    }
    return _url;
  }

  del(keys: string | string[] | '*' = '*', url = '') {
    let { preurl = '', params = {}, route = '' } = __url(url, this.type, this.filter);
    const _keys: any = isArray(keys) ? keys : [keys];
    if (keys === '*') {
      params = {};
    } else {
      _keys.forEach((key: any) => {
        delete params[key];
      });
    }
    const _url = __res(this.type, preurl, route, params);
    // 替换网址
    if (!url) {
      window.location.href = _url;
    }
    return _url;
  }

  get(opt: any = null, url = '') {
    let oKeys = this.all(url);
    if (isString(opt)) {
      return oKeys[opt];
    } else if (isArray(opt)) {
      let db = [];
      if (opt.length) {
        for (let i = 0, len = opt.length; i < len; i++) {
          let item = opt[i];
          db.push(oKeys[item]);
        }
      }
      return db;
    } else if (isObject(opt)) {
      let db: any = {};
      for (let item in opt) {
        if (opt.hasOwnProperty(item)) {
          db[item] = oKeys[opt[item]];
        }
      }
      return db;
    } else {
      console.error('param error');
    }
  }

  has(key: string = '', url = '') {
    return this.all(url).hasOwnProperty(key);
  }
}
export const search = new UrlParam({ type: '?', filter: true });
export const hash = new UrlParam({ type: '#', filter: true });
export default UrlParam;
