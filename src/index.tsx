// 样式
import './assets/css/main.scss';
import '@fekit/toast/theme/toast@ac.scss';
import 'swiper/css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';
import actions from './actions';
import store from '@fekit/react-store';
import Root from './root';
import { LayerView } from '@fekit/react-layer';

ReactDOM.render(
  <Provider store={store(actions)}>
    <HashRouter>
      <Root />
      <LayerView id="root" />
    </HashRouter>
  </Provider>,
  document.getElementById('root')
);
