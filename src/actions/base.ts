import { call, put, all } from 'redux-saga/effects';
import axios from 'axios';
import Toast from '@fekit/toast';

const error = new Toast({ theme: 'ac', icon: 'error', time: 2000 });

// 默认数据
export default {};

// 请求用户接口
export function* HOME_GET_BASE({ body: { lang = 'zh_cn' } = {}, then = () => {} } = {}): any {
  const res: any = yield call(axios.post, `/api?fun=get&mod=base${lang ? '&lang=' + lang : ''}`);
  console.log('HOME_GET_BASE', res);
  const { code = 0, data = {} } = res.data;
  if (code === 0) {
    yield put({
      type: '$',
      data: {
        key: 'base',
        val: { ...data, ...{ ready: 1 } }
      }
    });
  }
  then();
}

// 登录注册接口
export function* HOME_SSO_LOGIN({ body: { lang = 'zh_cn', username = '', password = '' } = {}, then = () => {} } = {}): any {
  const res: any = yield call(axios, '/api', {
    params: { fun: 'sso', mod: 'login', lang, username, password, remember: '0' }
  });
  console.log('HOME_SSO_LOGIN', res);
  const { code = 0, data: { logged = 0 } = {}, note = '' } = res.data;
  if (code === 0 && logged === 1) {
    // 登录成功刷新基础数据
    yield all([call(HOME_GET_BASE)]);
  } else {
    error.show({ text: note });
  }
}
