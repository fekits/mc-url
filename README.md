# npm-frameword

`NPM-FRAMEWORD` 是一款 NPM 包开发打包脚手架，外层是一个基于 [create-react-app](https://github.com/facebook/create-react-app) 的 `react` 项目脚手架，直接引用 `src/npm` 中的 `NPM`包源文件用来开发调试插件和写演示用的。`src/npm`是一个单独的基于 [tsdx](https://github.com/formium/tsdx) 的 `NPM` 打包脚手架。`NPM` 包的开发和发布都在这个目录下进行。

## 文件目录

```
.
├── build
├── public
├── src                                // 外层是用来调试NPM包和生成DEOM的
│   ├── assets
│   ├── npm                            // 这个才是NPM包所在目录，NPM 包的开发和发布都在这个目录下进行。
│   │   ├── LICENSE
│   │   ├── README.md
│   │   ├── dist
│   │   ├── package.json
│   │   ├── src
│   │   ├── tsconfig.json
│   │   └── yarn.lock
│   ├── reportWebVitals.ts
│   ├── root.test.tsx
│   ├── root.tsx
│   ├── setupTests.ts
│   ├── react-app-env.d.ts
│   └── index.tsx
│  
├── config-overrides.js
├── README.md
├── package.json
├── tsconfig.json
└── yarn.lock
```

## 可用脚本

在外层项目目录中，您可以运行：

### 开发调式

```
yarn start
```

或

```
npm run start
```

在开发模式下运行应用程序，用来调式 `NPM` 插件或编写基于 `NPM` 包的演示文档。项目运行后可以在浏览器中打开 [http://localhost:3000](http://localhost:3000) 查看。如果您进行编辑，页面将重新加载。您还将在控制台中看到任何 lint 错误。

<br>

### 单元测试

```
yarn test
```

或

```
npm run test
```

在交互式观察模式下启动测试运行器。有关更多信息，请参阅有关 [运行测试](https://facebook.github.io/create-react-app/docs/running-tests) 的部分。

<br>

### 打包预发

```
yarn build
```

或

```
npm run build
```

将用于生产的应用程序构建到 `build` 文件夹。它在生产模式下正确地捆绑了 React 并优化了构建以获得最佳性能。构建被缩小，文件名包括哈希值。您的应用程序已准备好部署！

有关更多信息，请参阅有关 [deployment](https://facebook.github.io/create-react-app/docs/deployment) 的部分。

<br>

### 打包上线

```
yarn babel
```

或

```
npm run babel
```

用于上线前的打包，包含了 `yarn build` 所做的工作，但同时清除了注释和 alert、console 等日志，同时无损压缩图，生成 ZIP 压缩包等

<br>

## 了解更多

您可以在 [create-react-app](https://facebook.github.io/create-react-app/docs/getting-started) 中了解更多信息。

要学习 React，请查看 [React 文档](https://reactjs.org/)。
<br>
<br>

## 打包发布 NPM 包

NPM 包在 [src/npm](./src/npm) 目录下，NPM 包文件都放在这个目录中，有单独的脚本打包 NPM 包。

1、在终端运行 `cd src/npm` 进入 `src/npm` 目录。

```
cd src/npm
```

2、然后运行 `yarn build` 打包 NPM 包。

```
yarn build
```

或

```
npm run build
```

3、修改`src/npm/package.json` 中的 `version`,根据具体情况递增一个版本号。可参考 [语义化版本规范(Semantic Versioning)](https://semver.org/lang/zh-CN/)

```js
{
  "name": "@za/zafe-xxx",  // NPM 包名
  "version": "0.x.xxx",    // NPM 包版本
  "author": "you name",    // 开发者署名
  "dependencies": {}       // NPM 包用到的依赖
}
```

3、然后运行 `npm publish` 发布到 `NPM`。

```
npm publish
```

如果是第一次发布，可能需要

```
npm publish --access public
```
