import { Random } from 'mockjs';

export default {
  'get|/api/user': (e) => {
    const res = {
      code: 0,
      message: '操作成功', // aaa
      data: {
        uuid: Random.id()
      }
    };
    return res;
  }
};
