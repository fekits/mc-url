import { Random } from 'mockjs';

export default {
  'get|/api/conf': (e) => {
    const res = {
      code: 0,
      message: '操作成功', // aaa
      data: {
        color: Random.color()
      }
    };

    return res;
  }
};
